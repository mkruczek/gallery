package pl.sdacademy.gallery;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity {


    public static final int RESULT_LOAD_IMAGE = 1;
    public static final int CAMERA_REQUEST = 2;
    public static final String IMAGE = "image_resource";
    private static final String TAG = "kru";

    Button button;
    ImageView imageView;
    Button aparat;
    static String resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resources = "empty";

        button = (Button) findViewById(R.id.button);
        aparat = (Button) findViewById(R.id.aparat);
        imageView = (ImageView) findViewById(R.id.imageView);

        if(savedInstanceState != null){
            resources = savedInstanceState.getString(IMAGE);
            Glide.with(getApplicationContext()).load(resources).override(900, 700).into(imageView);
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if ((ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE))) {
                        showExplanation("Potrzebujemy pozwolenia", "Daj je nam bym załadował foto",
                                Manifest.permission.READ_EXTERNAL_STORAGE, RESULT_LOAD_IMAGE);
                    } else {
                        // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                        requestPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, RESULT_LOAD_IMAGE);
                    }
                } else {
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(Intent.createChooser(i, "teraz wybiorę.."), RESULT_LOAD_IMAGE);
                }

            }
        });

        aparat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

    }

    private void requestPermissions(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this, new String[]{permissionName}, permissionRequestCode);
    }


    private void showExplanation(String title, String message, final String permission, final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                requestPermissions(permission, permissionRequestCode);
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bundle kundel = new Bundle();
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            resources = "file://" + picturePath;
            Glide.with(getApplicationContext()).load(resources).override(900, 700).into(imageView);


//            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        } else if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {

            String photoPath = "";

            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_ADDED, MediaStore.Images.ImageColumns.ORIENTATION}, MediaStore.Images.Media.DATE_ADDED, null, "date_added ASC");
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    Uri uri = Uri.parse(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA)));
                    photoPath = uri.toString();
                } while (cursor.moveToNext());
                cursor.close();
            }
            resources = "file://" + photoPath;
            Glide.with(getApplicationContext()).load(resources).override(900, 700).into(imageView);

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "jestem w onSaveInstanceState");
        Log.d(TAG, resources);
        outState.putString(IMAGE, resources);
        super.onSaveInstanceState(outState);
    }
}
